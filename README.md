# aonprdscraper

Scrapes the Archives of Nethys (aonprd.com) to get all of the spells.

If you can, please use the generated JSON file instead of running the script to save on bandwidth and hosting costs.

The generated data is primarily intended for use in the Traveling Spellbook app (https://gitlab.com/BumblingCoder/TravelingSpellbook/), but is provided separately to separate the CI, and in case it is useful to others.

The generated file is governed by the community use guidelines and Open Game License (see OGL.txt)

"This JSON game data uses trademarks and/or copyrights owned by Paizo Inc., which are used under Paizo's Community Use Policy. We are expressly prohibited from charging you to use or access this content. This JSON game data is not published, endorsed, or specifically approved by Paizo Inc. For more information about Paizo's Community Use Policy, please visit paizo.com/communityuse. For more information about Paizo Inc. and Paizo products, please visit paizo.com."
