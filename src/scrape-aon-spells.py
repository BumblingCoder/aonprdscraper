#!/bin/python3

from requests import get
from bs4 import BeautifulSoup
import json
import time
import re
import argparse
import pathlib

#The site returns 999 when using not setting a user agent.
userAgentHeader =  { 'user-agent' : 'Mozilla/5.0 (X11; Linux x86_64; rv:62.0) Gecko/20100101 Firefox/62.0' }

onlyparse = None
forcedownload = False
dataDir = ""

parser = argparse.ArgumentParser( description='Scrapes spell data from aonprd.com' )
parser.add_argument('--force-download', action='store_const', const=True, default=False, help='Force downloading html from aonprd.com instead of using html that has previously been downloaded' )
parser.add_argument('outputdir', help='Directory to store downloaded html and output json files')
options = parser.parse_args()

forcedownload = options.force_download
dataDir = pathlib.Path(options.outputdir)

def downloadPage( relativeurl ):
    htmlSource = get( f'http://aonprd.com/{relativeurl}', headers = userAgentHeader ).text
    return re.sub('<input.*/>', "", htmlSource )

def parseSpells():
    htmlSource = ""
    spellsPath = dataDir / 'all-spells.html'
    if forcedownload or not spellsPath.exists():
        htmlSource = downloadPage('Spells.aspx?Class=All')
        #work around a broken tag.
        htmlSource = htmlSource.replace('<hypnotism</i>','<i>hypnotism</i>')
        spellsPath.open('w').write(htmlSource)
    else:
        htmlSource = spellsPath.open().read()
    return parseSpellsHtml( htmlSource )

def parseSpellsHtml( htmlSource ):
    pageSoup = BeautifulSoup( htmlSource, 'html5lib' )
    spells = {}
    for item in pageSoup.table.findAll("span"):
        url = item.a['href']
        name = item.b.text.lstrip()
        description = item.text.split(': ')[1]
        if onlyparse is None or name == onlyparse:
            spells[ name ] = { 'description': description, 'url': url }
            parseSpellSpecifics( name, url, spells[name] )
    return spells

def parseSpellSpecifics( name, url, spell ):
    nameForFile = name.replace('/', '-')
    spellPath = dataDir / f'{nameForFile}.html'
    htmlSource = ""
    if forcedownload or not spellPath.exists():
        print( f'Downloading data for "{name}"')
        htmlSource = downloadPage( url )
        spellPath.open('w').write(htmlSource)
    else:
        print( f'Using previously downloaded data for "{name}"' )
        htmlSource = spellPath.open('r').read()
    pageSoup = BeautifulSoup( htmlSource, 'html5lib' )
    table = pageSoup.findAll( 'table', id="ctl00_MainContent_DataListTypes" )[0]
    spellNamesOnPage = table.findAll( 'h1' )
    for spellName in spellNamesOnPage:
        if not spellName.text.strip() == name:
            continue
        currentToken = spellName.next_sibling
        while currentToken is not None and currentToken.name != 'h1' and currentToken.name != 'h2':
            if currentToken.name == 'b':
                if currentToken.text == 'Source':
                    sources = []
                    currentToken = currentToken.next_sibling
                    while currentToken.name != 'b':
                        if hasattr( currentToken, 'text') :
                            if len(currentToken.text) > 0:
                                splitSources = currentToken.text.split(" pg. ")
                                sources.append( { "book": splitSources[0], 'page': splitSources[1] } )
                        currentToken = currentToken.next_sibling
                    spell['sources'] = sources
                elif currentToken.text == 'School':
                    currentToken = currentToken.next_sibling
                    spell['school'] = str(currentToken).strip(" ;")
                    currentToken = currentToken.next_sibling
                elif currentToken.text == 'Components':
                    currentToken = currentToken.next_sibling
                    spell['components'] = str(currentToken).strip(" ;")
                    currentToken = currentToken.next_sibling
                elif currentToken.text == 'Level':
                    currentToken = currentToken.next_sibling
                    spell['level'] = str(currentToken).strip(" ;")
                    currentToken = currentToken.next_sibling
                elif currentToken.text == 'Casting Time':
                    currentToken = currentToken.next_sibling
                    spell['castingtime'] = str(currentToken).strip(" ;")
                    currentToken = currentToken.next_sibling
                elif currentToken.text == 'Range':
                    currentToken = currentToken.next_sibling
                    spell['range'] = str(currentToken).strip(" ;")
                    currentToken = currentToken.next_sibling
                elif currentToken.text == 'Target':
                    currentToken = currentToken.next_sibling
                    spell['target'] = str(currentToken).strip(" ;")
                    currentToken = currentToken.next_sibling
                elif currentToken.text == 'Duration':
                    currentToken = currentToken.next_sibling
                    spell['duration'] = str(currentToken).strip(" ;")
                    currentToken = currentToken.next_sibling
                elif currentToken.text == 'Saving Throw':
                    currentToken = currentToken.next_sibling
                    spell['savingthrow'] = str(currentToken).strip(" ;")
                    currentToken = currentToken.next_sibling
                elif currentToken.text == 'Spell Resistance':
                    currentToken = currentToken.next_sibling
                    spell['resistance'] = str(currentToken).strip(" ;")
                    currentToken = currentToken.next_sibling
                elif currentToken.text == 'Area':
                    currentToken = currentToken.next_sibling
                    spell['area'] = str(currentToken).strip(" ;")
                    currentToken = currentToken.next_sibling
                elif currentToken.text == 'Effect':
                    currentToken = currentToken.next_sibling
                    spell['effect'] = str(currentToken).strip(" ;")
                    currentToken = currentToken.next_sibling
                elif currentToken.text == 'Targets':
                    currentToken = currentToken.next_sibling
                    spell['targets'] = str(currentToken).strip(" ;")
                    currentToken = currentToken.next_sibling
                else:
                    print( currentToken.name )
                    if hasattr( currentToken, 'text') :
                        print( currentToken.text )
                    currentToken = currentToken.next_sibling
            elif hasattr( currentToken, 'text') and currentToken.text == 'Description':
                description = ""
                currentToken = currentToken.next_sibling
                while currentToken and currentToken.name != 'h1' and currentToken.name != 'h2' and currentToken.name != 'h3':
                    if currentToken.name == 'table':
                        description += "\n\n"
                        for row in currentToken.findAll('tr'):
                            description += ', '.join([ td.text.strip() for td in row.findAll('td')])
                            description += "\n"
                        description += "\n"
                        currentToken = currentToken.next_sibling
                        continue
                    if currentToken.name == 'br':
                        description += '\n'
                    elif hasattr( currentToken, 'text') :
                        if len(currentToken.text) > 0:
                            description += str(currentToken.text)
                    else:
                        description += str(currentToken)
                        print("Using string representation")
                    currentToken = currentToken.next_sibling
                spell['rulestext'] = description
            else:
                currentToken = currentToken.next_sibling

if not dataDir.exists():
    dataDir.mkdir( parents=True, exist_ok=True )
spells = parseSpells()
f = (dataDir / 'spells-all.json').open( 'w' )
f.write( json.dumps( spells, sort_keys=True, indent=4 ) )
f.close()
